
def guid(path) {
    return path.replaceAll(/[:\/]/, "_")
}

Channel
    .fromPath( "s3://maccosslab/app/lakitu/public/pipeline_tests/test_lpmsconvert/smoke/msfiles.txt" )
    .splitText()
    .map{it.trim()}
    .map { tuple(guid(it), it, file(it)) }
    .set{ in_files }

msconvert_config = file( "s3://maccosslab-lakitudata/user_uploads/atkeller/20180629_thorium_DIA/raw/v5.0/crux/msconvert_config.txt" )

//in_files.subscribe onNext: { println it }, onComplete: { println 'Done' }

process read_files_list {

    /*
    This reads and caches the mzmls. Having this as a separate step allows
    for rerunning the pipeline without needing to retransfer files.
    */

    //container "alpine:3.9"
    container "ubuntu:latest"

    input:
    set fileId, filePath, file(file) from in_files

    output:
    set fileId, filePath, file(file) into out_read_files

    """
    """

}

//out_read_files.subscribe onNext: { println it }
out_read_files.into { out_read_files__mapper; out_read_files__runner }

process file_mapper {

    /*
    Mapper allows for conditionally applying different options or
     processing to each file
    */

    //container "python:3.7.3-alpine3.9"
    container "python:3.7.3"

    input:
    set fileId, filePath, file(file) from out_read_files__mapper

    output:
    set fileId, stdout into out_file_mapper

    """
    #!/usr/bin/env python3

    file_path="$filePath"
    name = file_path.split('/')[-1]

    print("{0:s}".format(name))
    """
}

out_file_mapper.subscribe onNext: { println it }

process msconvert {

    label 'bigTask'

    container 'chambm/pwiz-skyline-i-agree-to-the-vendor-licenses:3.0.19073-85be84641'
    //container 'job-definition://nf-chambm-pwiz-skyline-i-agree-to-the-vendor-licenses-3-0-19073-85be84641:2'

    memory '7168 MB'

    cpus 2

    // TODO containerOptions Not supported by AWS Batch, find another way...
    //containerOptions '-e "WINEDEBUG=-all"'

    input:
    set fileId, _, file(file) from out_read_files__runner
    file msc_config from msconvert_config

    output:
    set fileId, file("msconvert_out/*") into out_msconvert

    // TODO set a reasonable output filename
    """
    wine msconvert -vz $file -c $msc_config --outdir msconvert_out
    """
}

