# Nextflow MSConvert Pipeline

## Test

```
nextflow msconvert.nf \
  -bucket-dir s3://atkeller-nextflow/runs/msconvert.nf/smoke_test \
  --in_file_list s3://maccosslab/app/lakitu/public/pipeline_tests/test_lpmsconvert/smoke/msfiles.txt \
  --msconvert_config  s3://maccosslab/app/lakitu/public/pipeline_tests/test_lpmsconvert/smoke/msconvert_config.txt
```
